use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::env;
use std::ffi::OsString;
use std::fs;
use std::ops::{Add, Mul, Sub};
use std::process;
use std::rc::Rc;

fn main() {
    match env::args_os().nth(1) {
        Some(file_path) => {
            let input = parse_input(file_path);
            println!("{}", part1(&input));
            println!("{}", part2(&input));
        }
        None => {
            eprintln!("expected 1 argument, but got none");
            process::exit(1);
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
struct Vector(i32, i32);

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct Node {
    coord: Vector,
    direction: Vector,
    steps: u32,
    turns: u32,
    prev: Option<Rc<Node>>,
}

#[derive(Debug, Clone)]
struct NodeIterator {
    current: Option<Rc<Node>>,
}

type Input = HashMap<Vector, char>;

impl Vector {
    const UP: Self = Self(0, -1);
    const DOWN: Self = Self(0, 1);
    const LEFT: Self = Self(-1, 0);
    const RIGHT: Self = Self(1, 0);
}

impl Vector {
    fn rotate_clockwise(self) -> Self {
        Self(-self.1, self.0)
    }

    fn rotate_anticlockwise(self) -> Self {
        Self(self.1, -self.0)
    }

    fn magnitude(&self) -> f64 {
        ((self.0.pow(2) + self.1.pow(2)) as f64).sqrt()
    }

    fn manhattan(&self) -> i32 {
        self.0.abs() + self.1.abs()
    }
}

impl Add for Vector {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Node {
    fn new(coord: Vector, direction: Vector) -> Self {
        Self {
            coord,
            direction,
            steps: 0,
            turns: 0,
            prev: None,
        }
    }

    fn score(&self) -> u32 {
        self.turns * 1000 + self.steps
    }
}

impl Sub for Vector {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self(self.0 - rhs.0, self.1 - rhs.1)
    }
}

impl Mul<i32> for Vector {
    type Output = Self;

    fn mul(self, factor: i32) -> Self::Output {
        Self(self.0 * factor, self.1 * factor)
    }
}

impl NodeIterator {
    fn new(start: Option<Rc<Node>>) -> Self {
        Self { current: start }
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .score()
            .cmp(&self.score())
            .then(self.steps.cmp(&other.steps))
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl IntoIterator for Node {
    type Item = Rc<Node>;
    type IntoIter = NodeIterator;

    fn into_iter(self) -> Self::IntoIter {
        NodeIterator::new(Some(Rc::new(self)))
    }
}

impl Iterator for NodeIterator {
    type Item = Rc<Node>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(current) = &self.current {
            let prev = current.prev.clone();
            self.current = prev.clone();
            prev
        } else {
            None
        }
    }
}

fn parse_input(file_path: OsString) -> Input {
    fs::read_to_string(file_path)
        .unwrap()
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, c)| *c != '#')
                .map(move |(x, c)| (Vector(x as i32, y as i32), c))
        })
        .collect()
}

fn part1(input: &Input) -> usize {
    let &end_coord = input.iter().find(|(_, c)| **c == 'E').unwrap().0;
    let &start_coord = input.iter().find(|(_, c)| **c == 'S').unwrap().0;
    let mut stack = vec![(start_coord, 0)];
    let mut path = HashMap::new();
    while let Some((pos, time)) = stack.pop() {
        if path.contains_key(&pos) {
            continue;
        }
        if !input.contains_key(&pos) {
            continue;
        }
        path.insert(pos, time);
        for direction in [Vector::UP, Vector::DOWN, Vector::LEFT, Vector::RIGHT] {
            stack.push((pos + direction, time + 1))
        }
    }
    path.iter()
        .map(|(&start_pos, start_time)| {
            [Vector::UP, Vector::DOWN, Vector::LEFT, Vector::RIGHT]
                .iter()
                .map(|&direction| direction * 2 + start_pos)
                .filter(|end_pos| {
                    path.get(&end_pos)
                        .map(|end_time| {
                            let time_saved = end_time - start_time - 2;
                            if time_saved >= 100 {
                                println!(
                                    "{}: {},{} => {},{}",
                                    time_saved, start_pos.0, start_pos.1, end_pos.0, end_pos.1
                                );
                            }
                            time_saved >= 100
                        })
                        .unwrap_or(false)
                })
                .count()
        })
        .sum()
}

fn part2(input: &Input) -> usize {
    let &end_coord = input.iter().find(|(_, c)| **c == 'E').unwrap().0;
    let &start_coord = input.iter().find(|(_, c)| **c == 'S').unwrap().0;
    let mut stack = vec![(start_coord, 0)];
    let mut path = HashMap::new();
    while let Some((pos, time)) = stack.pop() {
        if path.contains_key(&pos) {
            continue;
        }
        if !input.contains_key(&pos) {
            continue;
        }
        path.insert(pos, time);
        for direction in [Vector::UP, Vector::DOWN, Vector::LEFT, Vector::RIGHT] {
            stack.push((pos + direction, time + 1))
        }
    }
    for y in 0..15 {
        for x in 0..15 {
            print!(
                "{}",
                path.get(&Vector(x, y))
                    .map(|time| (time % 10).to_string())
                    .unwrap_or(String::from(" "))
            );
        }
        println!();
    }
    println!();
    path.iter()
        .map(|(&start_pos, start_time)| {
            path.iter()
                .filter(|&(&end_pos, &end_time)| {
                    end_time - start_time - 2 >= 76 && (end_pos - start_pos).manhattan() <= 20
                })
                .count()
        })
        .sum()
}

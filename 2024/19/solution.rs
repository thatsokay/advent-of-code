use std::collections::{HashMap, HashSet};
use std::env;
use std::ffi::OsString;
use std::fs;
use std::process;

fn main() {
    match env::args_os().nth(1) {
        Some(file_path) => {
            let input = parse_input(file_path);
            println!("{}", part1(&input));
            println!("{}", part2(&input));
        }
        None => {
            eprintln!("expected 1 argument, but got none");
            process::exit(1);
        }
    }
}

#[derive(Debug, Clone)]
struct Cache<'a> {
    cache: HashMap<&'a str, u64>,
    patterns: &'a [String],
}

type Input = (Vec<String>, Vec<String>);

impl<'a> Cache<'a> {
    fn new(patterns: &'a [String]) -> Self {
        Self {
            cache: HashMap::new(),
            patterns,
        }
    }

    fn count_permutations(&mut self, design: &'a str) -> u64 {
        if design.is_empty() {
            return 1;
        }
        if let Some(&count) = self.cache.get(design) {
            return count;
        }
        let count = self
            .patterns
            .iter()
            .flat_map(|pattern| design.strip_prefix(pattern))
            .map(|stripped| self.count_permutations(stripped))
            .sum();
        self.cache.insert(design, count);
        count
    }
}

fn parse_input(file_path: OsString) -> Input {
    let content = fs::read_to_string(file_path).unwrap();
    let (patterns_content, designs_content) = content.split_once("\n\n").unwrap();
    let patterns = patterns_content
        .split(", ")
        .map(|s| s.to_string())
        .collect();
    let designs = designs_content.lines().map(|s| s.to_string()).collect();
    (patterns, designs)
}

fn part1(input: &Input) -> usize {
    input
        .1
        .iter()
        .filter(|design| {
            let mut stack = vec![&design[..]];
            let mut visited = HashSet::new();
            while let Some(remaining) = stack.pop() {
                if remaining.is_empty() {
                    return true;
                }
                if visited.contains(remaining) {
                    continue;
                }
                visited.insert(remaining);
                for stripped in input
                    .0
                    .iter()
                    .flat_map(|pattern| remaining.strip_prefix(pattern))
                {
                    stack.push(stripped);
                }
            }
            return false;
        })
        .count()
}

fn part2(input: &Input) -> u64 {
    let mut cache = Cache::new(&input.0);
    input
        .1
        .iter()
        .map(|design| cache.count_permutations(design))
        .sum()
}
